class AddressJob
  include SuckerPunch::Job

  def perform( property )
    sleep 20

    state = State.find_by_initial( property.location["state"] )
    proccess_city( property, state )
  end

  protected
    def proccess_city( property, state )
      city = City.where( :name => property.location["city"], :state => state )

      if city.empty?
        city = save_city( property, state )
        save_district( property, city )

      else
        proccess_district( property, city[0] )
      end
    end

    def proccess_district( property, city )
      district = District.where( :name => property.location["district"], :city => city )

      if district.empty?
        save_district( property, city)
      end
    end

    def save_city( property, state )
      city = City.new
      city.name = property.location["city"]
      city.state = state
      city.save
      city
    end

    def save_district( property, city )
      district = District.new
      district.name = property.location["district"]
      district.city = city
      district.save
    end
end
