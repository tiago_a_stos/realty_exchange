class Photo < ApplicationRecord
  belongs_to :property

  has_attached_file :image, :styles => { thumb: "90x90", small: "226x160#", large: "350x350>" }
  validates_attachment :image, content_type: { content_type: ["image/jpeg", "image/gif", "image/png"] }

  def as_json(options={})
    { image: self.image.url(:small) }
  end
end
