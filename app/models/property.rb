class Property < ApplicationRecord
  belongs_to :user
  has_many :photos, :dependent => :destroy

  def search( params )
    query_string = ""
    query_params = Hash.new

    location_params = Hash.new
    unless params[:state].nil? || params[:state].blank?
      state = State.find( params[:state].to_i )
      location_params[:state] = state.initial
    end

    unless params[:city].nil? || params[:city].blank?
      city = City.find( params[:city].to_i )
      location_params[:city] = city.name
    end

    unless params[:district].nil? || params[:district].blank?
      district = District.find( params[:district].to_i )
      location_params[:district] = district.name
    end

    unless params[:zone].nil? || params[:zone].blank?
      location_params[:zone] = params[:zone]
    end

    p '´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´'
    p location_params
    p '´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´'
    unless location_params.empty?
      p 'location_params não está vazio'
      query_string += "location @> :location_p"
      query_params[:location_p] = location_params.to_json
    else
      p 'location_params está vazio'
    end

    kind_params = Hash.new
    unless params[:utilization].nil? || params[:utilization].blank?
      kind_params[:utilization] = params[:utilization]
    end

    unless params[:type].nil? || params[:type].blank?
      kind_params[:type] = params[:type]
    end

    unless kind_params.empty?
      unless location_params.empty?
        query_string += " and "
      end
      query_string += "kind @> :kind_p"
      query_params[:kind_p] = kind_params.to_json
    end

    caracteristics_params = Hash.new
    unless params[:dormitories_number].nil? || params[:dormitories_number].blank?
      caracteristics_params[:dormitories_number] = params[:dormitories_number].to_i
    end
    unless params[:suites_number].nil? || params[:suites_number].blank?
      caracteristics_params[:suites_number] = params[:suites_number].to_i
    end
    unless params[:parking_spaces].nil? || params[:parking_spaces].blank?
      caracteristics_params[:parking_spaces] = params[:parking_spaces].to_i
    end

    unless caracteristics_params.empty?
      unless location_params.empty? && kind_params.empty?
        query_string += " and "
      end
      query_string += "caracteristics @> :caracteristics_p"
      query_params[:caracteristics_p] = caracteristics_params.to_json
    end

    # ´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´
    # TOTAL AREA PARAMS
    # ´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´
    unless params[:total_area_min].nil? || params[:total_area_min].blank?
      unless query_string.empty?
        query_string += " and "
      end
      query_string += "(footage ->> 'total_area')::int >= :total_area_min"
      query_params[:total_area_min] = params[:total_area_min]
    end
    unless params[:total_area_max].nil? || params[:total_area_max].blank?
      unless query_string.empty?
        query_string += " and "
      end
      query_string += "(footage ->> 'total_area')::int <= :total_area_max"
      query_params[:total_area_max] = params[:total_area_max]
    end

    # ´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´
    # USEFUL AREA PARAMS
    # ´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´
    unless params[:useful_area_min].nil? || params[:useful_area_min].blank?
      unless query_string.empty?
        query_string += " and "
      end
      query_string += "(footage ->> 'useful_area')::int >= :useful_area_min"
      query_params[:useful_area_min] = params[:useful_area_min]
    end
    unless params[:useful_area_max].nil? || params[:useful_area_max].blank?
      unless query_string.empty?
        query_string += " and "
      end
      query_string += "(footage ->> 'useful_area')::int <= :useful_area_max"
      query_params[:useful_area_max] = params[:useful_area_max]
    end

    # ´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´
    # CARPET AREA PARAMS
    # ´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´
    unless params[:carpet_area_min].nil? || params[:carpet_area_min].blank?
      unless query_string.empty?
        query_string += " and "
      end
      query_string += "(footage ->> 'carpet_area')::int >= :carpet_area_min"
      query_params[:carpet_area_min] = params[:carpet_area_min]
    end
    unless params[:carpet_area_max].nil? || params[:carpet_area_max].blank?
      unless query_string.empty?
        query_string += " and "
      end
      query_string += "(footage ->> 'carpet_area')::int <= :carpet_area_max"
      query_params[:carpet_area_max] = params[:carpet_area_max]
    end


    # ´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´
    # FRONT FOOTAGE PARAMS
    # ´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´
    unless params[:front_footage_min].nil? || params[:front_footage_min].blank?
      unless query_string.empty?
        query_string += " and "
      end
      query_string += "(footage ->> 'front_footage')::int >= :front_footage_min"
      query_params[:front_footage_min] = params[:front_footage_min]
    end
    unless params[:front_footage_max].nil? || params[:front_footage_max].blank?
      unless query_string.empty?
        query_string += " and "
      end
      query_string += "(footage ->> 'front_footage')::int <= :front_footage_max"
      query_params[:front_footage_max] = params[:front_footage_max]
    end

    # ´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´
    # FRONT FOOTAGE PARAMS
    # ´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´
    unless params[:back_footage_min].nil? || params[:back_footage_min].blank?
      unless query_string.empty?
        query_string += " and "
      end
      query_string += "(footage ->> 'back_footage')::int >= :back_footage_min"
      query_params[:back_footage_min] = params[:back_footage_min]
    end
    unless params[:back_footage_max].nil? || params[:back_footage_max].blank?
      unless query_string.empty?
        query_string += " and "
      end
      query_string += "(footage ->> 'back_footage')::int <= :back_footage_max"
      query_params[:back_footage_max] = params[:back_footage_max]
    end

    properties = Property.where( query_string, # "location @> :location_p and kind @> :kind_p and caracteristics @> :caracteristics_p", # and (footage ->> 'total_area')::int < ?",
        query_params ) #, 12 )
  end
end
