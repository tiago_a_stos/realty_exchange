class CityController < ApplicationController
  before_action :authenticate_user!, :except => [:list, :get_by_state]

  def create
    city = City.new
    city.name = city_params[:name]
    city.state = State.find( city_params[:state].to_i )

    if city.save
      render :json => city, :status => :ok
    else
      render :json => { :message => "Erro Interno" }, :status => :internal_server_error
    end
  end

  def list
    cities = City.all
    render :json => cities, :include => {:state => {:only => :name}}, :except => [:created_at, :updated_at], :status => :ok
  end

  def get_by_state
    cities = City.where( :state => params[:state_id].to_i )
    render :json => cities, :status => :ok
  end

  private
    def city_params
      params.require(:city).permit( :name, :state )
    end
end
