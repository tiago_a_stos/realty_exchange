class User::SessionsController < Devise::SessionsController

  respond_to :json

  # GET /resource/sign_in
  def new
    super
  end

  # POST /resource/sign_in
  def create
    resource = User.find_for_database_authentication(:email=>params[:user][:email])
    return invalid_login_attempt unless resource

    if resource.valid_password?(params[:user][:password])
      sign_in("user", resource)
      render :json => { :success => true, :email => resource.email, :location => after_sign_in_path_for(resource) }, status => 200
      return
    end

    invalid_login_attempt
  end


  protected
    def ensure_params_exist
      return unless params[:user].blank?
      render :json => {:success => false, :message => "missing user_login parameter"}, :status => 422
    end

    def invalid_login_attempt
      warden.custom_failure!
      render :json => {:success=>false, :message=>"Email e/ou seha inválido."}, :status => 401
    end

    def user_params( user )
      params.require(:user).permit(:email, :password)
    end
end
