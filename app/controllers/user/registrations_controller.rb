class User::RegistrationsController < Devise::RegistrationsController
# before_action :configure_sign_up_params, only: [:create]
# before_action :configure_account_update_params, only: [:update]
  respond_to :json

  # POST /resource
  def create
    build_resource(sign_up_params)

    begin
      resource.validate!
    rescue Exception => e
      render :json => { :success => false, :email => resource.email, :message => e.message }, status => 400
      return
    end

    resource.save

    yield resource if block_given?

    if resource.persisted?
      if resource.active_for_authentication?
        set_flash_message! :notice, :signed_up
        sign_up(resource_name, resource)
        render :json => { :success => true, :email => resource.email, :location => after_sign_up_path_for(resource) }, status => 200

      else
        set_flash_message! :notice, :"signed_up_but_#{resource.inactive_message}"
        expire_data_after_sign_in!
        #respond_with resource, location: after_inactive_sign_up_path_for(resource)
        render :json => { :success => true, :email => resource.email, :location => after_sign_up_path_for(resource) }, status => 200
      end

    else
      clean_up_passwords resource
      set_minimum_password_length
      #respond_with resource
      render :json => { :success => false, :email => resource.email, :location => after_sign_up_path_for(resource) }, status => 500
    end
  end

  protected
    def sign_up_params
      params[:registration][:cpf].gsub!(".", "")
      params[:registration][:cpf].gsub!("-", "")
      params.require(:registration).permit(:email, :password, :password_confirmation, :name, :last_name, :gender, :rg, :cpf, :marital_status, :alternative_email, :home_phone, :commercial_phone, :cell_phone, :cell_phone2)
    end

    def confirmation_equals?
      sign_up_params[:password].eql? sign_up_params[:password_confirmation]
    end

    def different_passwords_error
      render :json => {:success=>false, :message=>"Confirmação de senha não confere."}, :status => 400
    end
end
