class PropertyController < ApplicationController
  before_action :authenticate_user!, :except => [:list, :show, :photo_upload, :search]

  def create
    property = Property.new( property_params )
    property.user = current_user

    if property.save
      AddressJob.perform_async( property )
      render :json => property, :status => :ok
    else
      render :json => { :message => "Erro Interno" }, :status => :internal_server_error
    end
  end

  def show
    begin
      property = Property.find(params[:id])
      AddressJob.perform_async( property )
      render :json => property, :include => [:photos], :status => :ok
    rescue Exception => e
      render :json => property, :status => :not_found
    end
  end

  def photos
    begin
      property = Property.find(params[:id])
      render :json => property.photos.as_json, :status => :ok
    rescue Exception => e
      render :json => property, :status => :not_found
    end
  end

  def list_by_user
    properties = Property.where( :user => current_user )
    render :json => properties, :status => :ok
  end

  def list
    properties = Property.all
    render :json => properties, :status => :ok
  end

  def photo_upload
    property = Property.find( params[:id].to_i )

    photo = Photo.new
    photo.image = params[:file]
    property.photos << photo

    if property.save
      render :json => { :message => "OK" }, :status => :ok
    else
      render :json => { :message => "Erro Interno" }, :status => :internal_server_error
    end
  end

  def search
    p '-----------------------------------------------------------------------------'
    p params
    p '-----------------------------------------------------------------------------'

    properties = Property.new.search( params )
    render :json => properties, :status => :ok
  end

  protected
    def property_params
      params.require(:property).permit!
    end
end
