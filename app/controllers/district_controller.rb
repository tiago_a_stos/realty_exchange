class DistrictController < ApplicationController
  before_action :authenticate_user!, :except => [:list, :get_by_city]

  def create
    district = District.new
    district.name = district_params[:name]
    district.city = City.find( district_params[:city].to_i )

    if district.save
      render :json => district, :status => :ok
    else
      render :json => { :message => "Erro Interno" }, :status => :internal_server_error
    end
  end

  def list
    districts = District.all
    render :json => districts,
      :include => { :city => { :include => { :state => {:only => :initial }}, :only => :name } },
      :except => [:created_at, :updated_at], :status => :ok
  end

  def get_by_city
    p '-----------------------------------'
    p params
    p '-----------------------------------'
    districts = District.where( :city => params[:city_id].to_i )
    render :json => districts, :status => :ok
  end

  private
    def district_params
      params.require(:district).permit( :name, :city )
    end
end
