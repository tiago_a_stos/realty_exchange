class PropertyIntendedController < ApplicationController
  before_action :authenticate_user!

  def create
    property_intended = PropertyIntended.new( property_intended_params )
    property_intended.user = current_user

    if property_intended.save
      render :json => property_intended, :status => :ok
    else
      render :json => { :message => "Erro Interno" }, :status => :internal_server_error
    end
  end

  def show
    p params[:id]
    begin
      property = PropertyIntended.find(params[:id])

      district = District.find( property.location["district"].to_i )
      property.location["district"] = {:id => district.id, :name => district.name}
      property.location["city"] = {:id => district.city.id, :name => district.city.name}
      property.location["state"] = {:id => district.city.state.id, :name => district.city.state.name}

      p '----------------------------------------'
      p property.to_json
      p '----------------------------------------'

      render :json => property, :status => :ok
    rescue Exception => e
      p e
      render :json => property, :status => :not_found
    end
  end

  def list_by_user
    properties = PropertyIntended.where( :user => current_user )
    render :json => properties, :status => :ok
  end

  protected
    def property_intended_params
      params.require(:property_intended).permit!
    end
end
