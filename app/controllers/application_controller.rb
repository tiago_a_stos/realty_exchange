class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  layout :layout_by_resource

  def after_sign_in_path_for(resource)
    p "---> Redirecionando após login"
    "/dashboard"
  end

  def after_sign_out_path_for(resource_or_scope)
    p "---> Redirecionando após logoff"
    "/"
  end

  protected

  def layout_by_resource
    if user_signed_in?
      p '<><><><> Usuário Logado <><><><>'
      if current_user.try(:admin?)
        p '---> Layout para usuário Admin'
        "application_admin"
      else
        p '---> Layout para usuário'
        "application_user"
      end
    else
      p '<><><><> Usuário não Logado <><><><>'
      "application"
    end
  end

end
