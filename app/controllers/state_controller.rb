class StateController < ApplicationController
  before_action :authenticate_user!, :except => [:list]

  def create
    state = State.new( state_params )

    if state.save
      render :json => state, :status => :ok
    else
      render :json => { :message => "Erro Interno" }, :status => :internal_server_error
    end
  end

  def list
    states = State.all
    render :json => states, :status => :ok
  end

  private
    def state_params
      params.require(:state).permit( :name, :initial )
    end
end
