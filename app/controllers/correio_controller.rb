class CorreioController < ApplicationController

  def get_address
    postal_code = params[:postal_code].gsub( "-", "" )
    address = Correios::CEP::AddressFinder.get( postal_code )

    render :json => address, :status => :ok
  end

end
