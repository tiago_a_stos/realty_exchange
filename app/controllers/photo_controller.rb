class PhotoController < ApplicationController
  def main_photo
    begin
      photo = Photo.where( :property => params[:property_id] ).first
      render :json => photo.as_json, :status => :ok
    rescue Exception => e
      render :json => "Error", :status => :not_found
    end
  end
end
