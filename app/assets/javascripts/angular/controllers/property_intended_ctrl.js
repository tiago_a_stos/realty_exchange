function PropertyIntendedCtrl($scope, $http, $location, propertyKind) {
	$scope.property = {};

	$http.get( '/user/signed_in' )
		.success( function( data ) {
			console.log( "Logado ");
		})
		.error( function( data ) {
			window.location = '/';
		});


	/**
	 * CAMPOS MASCARADODS
	 */
	VMasker( $("[type|='number']") ).maskNumber();
	VMasker( $("#value_sale") ).maskMoney();
	VMasker( $("#value_condo") ).maskMoney();
	VMasker( $("#value_tax") ).maskMoney();

	/**
	 * SAVING THE PROPERTY
	 */
	$scope.save = function() {
		if (!$scope.validate()) {
			return;
		}

		var property = {property_intended: $scope.property};

		$http.post('/property_intended', property)
			.success( function( data ) {
				console.log( data );
				$location.path( "/dashboard" );
			})
			.error( function( data ) {
				console.log( data );
			});
	}

	$scope.validate = function() {
		$scope.utilization_not_selected = false;
		$scope.type_not_selected = false;
		$scope.district_ni = false;
		$scope.city_ni = false;
		$scope.state_ni = false;

		$scope.parking_spaces_ni = false;
		$scope.suites_number_ni = false;
		$scope.dormitories_number_ni = false;

		validated = true;

		if ( $scope.property.kind == null ) {
			$scope.utilization_not_selected = true;
			$scope.type_not_selected = true;
			validated = false;
			console.log( "kind" );

		} else {
			if ( $scope.property.kind.utilization == null || $scope.property.kind.utilization == "" ) {
				$scope.utilization_not_selected = true;
				validated = false;
				console.log( "utilization" );
			}

			if ( $scope.property.kind.type == null || $scope.property.kind.type == "" ) {
				$scope.type_not_selected = true;
				validated = false;
				console.log( "type" );
			}
		}

		if ( $scope.property.location == null ) {
			validated = false;
			console.log( "location" );

		} else {
			if ( $scope.property.location.district == null ) {
				$scope.district_ni = true;
				validated = false;
				console.log( "district" );
			}

			if ( $scope.property.location.city == null ) {
				$scope.city_ni = true;
				validated = false;
				console.log( "city" );
			}

			if ( $scope.property.location.state == null ) {
				$scope.state_ni = true;
				validated = false;
				console.log( "state" );
			}
		}

		if ( $scope.property.caracteristics == null ) {
			if ( $scope.disable_dormitories ) {
				$scope.dormitories_number_ni = false;
			} else {
				$scope.dormitories_number_ni = true;
			}

			if ( $scope.disable_suites ) {
				$scope.suites_number_ni = false;
			} else {
				$scope.suites_number_ni = true;
			}
			
			$scope.parking_spaces_ni = true;
			validated = false;
			console.log( "caracteristics" );

		} else {
			if ( $scope.property.caracteristics.dormitories_number == null ) {
				if ( $scope.disable_dormitories ) {
					$scope.dormitories_number_ni = false;
				} else {
					$scope.dormitories_number_ni = true;
					validated = false;
					console.log( "dormitories_number" );
				}
			}

			if ( $scope.property.caracteristics.suites_number == null ) {
				if ( $scope.disable_suites ) {
					$scope.suites_number_ni = false;
				} else {
					$scope.suites_number_ni = true;
					validated = false;
					console.log( "suites_number" );
				}
			}

			if ( $scope.property.caracteristics.parking_spaces == null ) {
				$scope.parking_spaces_ni = true;
				validated = false;
				console.log( "parking_spaces" );
			}
		}
		console.log( validated );
		return validated;
	};

	/**
	 * END SAVING THE PROPERTY
	 */


	/**
	 * DEFINIÇÃO DE CAMPOS POR TIPO DE IMÓVEL
	 **/

	$scope.options_type = ["Seleciona uma utilização"]

	$scope.define_types = function() {
		$scope.options_type = propertyKind( $scope.property.kind.utilization );
		console.log($scope.options_type);

		$scope.unit_details_per_utilization = [];
		for ( detail_index in $scope.unit_details ) {
			detail = $scope.unit_details[detail_index];
			utilizations = detail.utilizations;
			position = utilizations.indexOf( $scope.property.kind.utilization );

			if ( position != -1 ) {
				$scope.unit_details_per_utilization.push( detail );
			}
		}

		$scope.enterprise_details_per_utilization = [];
		for ( detail_index in $scope.enterprise_details ) {
			detail = $scope.enterprise_details[detail_index];
			utilizations = detail.utilizations;
			position = utilizations.indexOf( $scope.property.kind.utilization );

			if ( position != -1 ) {
				$scope.enterprise_details_per_utilization.push( detail );
			}
		}


		if ( $scope.property.kind.utilization == "RESIDENCIAL" ) {
			// Características
			$scope.disable_dormitories = false;
			$scope.disable_suites = false;
			$scope.disable_units_per_floor = false;
			$scope.disable_floors_number = false;
			$scope.disable_face = false;
			// Metragem
			$scope.disable_carpet_area = true;
			$scope.disable_office_area = true;
			$scope.disable_support_area = true;
			$scope.disable_patio_area = true;
			$scope.disable_manufacturing_area = true;
			$scope.disable_celing_height = true;
			$scope.disable_front_footage = true;
			$scope.disable_back_footage = true;
			$scope.disable_left_footage = true;
			$scope.disable_right_footage = true;

		}

		if ( $scope.property.kind.utilization == "COMERCIAL" ) {
			// Características
			$scope.disable_dormitories = true;
			$scope.disable_suites = true;
			$scope.disable_face = true;
			$scope.disable_units_per_floor = false;
			$scope.disable_floors_number = false;
			// Metragem
			$scope.disable_carpet_area = false;
			$scope.disable_office_area = false;
			$scope.disable_support_area = false;
			$scope.disable_patio_area = true;
			$scope.disable_manufacturing_area = true;
			$scope.disable_celing_height = false;
			$scope.disable_front_footage = false;
			$scope.disable_back_footage = false;
			$scope.disable_left_footage = false;
			$scope.disable_right_footage = false;
		}

		if ( $scope.property.kind.utilization == "INDUSTRIAL" ) {
			// Características
			$scope.disable_dormitories = true;
			$scope.disable_suites = true;
			$scope.disable_units_per_floor = true;
			$scope.disable_floors_number = false;
			$scope.disable_face = true;
			// Metragem
			$scope.disable_carpet_area = false;
			$scope.disable_office_area = false;
			$scope.disable_support_area = false;
			$scope.disable_patio_area = false;
			$scope.disable_manufacturing_area = false;
			$scope.disable_celing_height = false;
			$scope.disable_front_footage = false;
			$scope.disable_back_footage = false;
			$scope.disable_left_footage = false;
			$scope.disable_right_footage = false;
		}

		if ( $scope.property.kind.utilization == "RURAL" ) {
			// Características
			$scope.disable_dormitories = false;
			$scope.disable_suites = false;
			$scope.disable_units_per_floor = true;
			$scope.disable_floors_number = true;
			$scope.disable_face = true;
			// Metragem
			$scope.disable_carpet_area = true;
			$scope.disable_office_area = true;
			$scope.disable_support_area = true;
			$scope.disable_patio_area = true;
			$scope.disable_manufacturing_area = true;
			$scope.disable_celing_height = true;
			$scope.disable_front_footage = false;
			$scope.disable_back_footage = false;
			$scope.disable_left_footage = false;
			$scope.disable_right_footage = false;
		}

		$scope.validate();
	};

	$scope.get_states = function() {
		$http.get( '/states/all' ).success( function( states ) {
			$scope.states = states;
		});
	};

	$scope.get_cities = function() {	  
		$http.get( '/cities/by_state/' + $scope.property.location.state ).success( function( cities ) {
			$scope.cities = cities;
		});
	}

  $scope.get_districts = function() {
		$http.get( '/districts/by_city/' + $scope.property.location.city ).success( function( districts ) {
			$scope.districts = districts;
		});
	}

	$scope.get_states();
}

realtyExchangeApp.controller('PropertyIntendedCtrl', ['$scope', '$http', '$location', 'propertyKind', PropertyIntendedCtrl]);
