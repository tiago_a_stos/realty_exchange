function PropertiesResultsCtrl($scope, $http, $location, $routeParams, propertyKind, getMainPhoto) {
	var search = $location.search();
	$scope.search = {};

	$scope.get_properties = function() {
		$http.get( '/properties/search?state=' + ( $scope.search.state || "") +
							"&city=" + ( $scope.search.city || "" ) +
							"&district=" + ( $scope.search.district || "" ) +
							"&zone=" + ( $scope.search.zone || "" ) +
							"&utilization=" + ( $scope.search.utilization || "" ) +
							"&type=" + ( $scope.search.property_type || "" ) +
							"&dormitories_number=" + ( $scope.search.dormitories_number || "" ) +
							"&suites_number=" + ( $scope.search.suites_number || "" ) +
							"&parking_spaces=" + ( $scope.search.parking_spaces || "" ) +
							"&total_area_min=" + ( $scope.search.total_area_min || "" ) + "&total_area_max=" + ( $scope.search.total_area_max || "" ) +
							"&useful_area_min=" + ( $scope.search.useful_area_min || "" ) + "&useful_area_max=" + ( $scope.search.useful_area_max || "" ) +
							"&carpet_area_min=" + ( $scope.search.carpet_area_min || "" ) + "&carpet_area_max=" + ( $scope.search.carpet_area_max || "" ) +
							"&front_footage_min=" + ( $scope.search.front_footage_min || "" ) + "&front_footage_max=" + ( $scope.search.front_footage_max || "" ) +
							"&back_footage_min=" + ( $scope.search.back_footage_min || "" ) + "&back_footage_max=" + ( $scope.search.back_footage_max || "" )  )
			.success( function( properties ) {
				
				if ( properties.length < 1 ) {
					$scope.no_result = true;
				} else {
					$scope.no_result = false;
				}
			  $scope.properties = getMainPhoto( properties );
			});
	};

  $scope.get_states = function() {
		$http.get( '/states/all' ).success( function( states ) {
			$scope.states = states;
			if ( search.state ) {
				$scope.search["state"] = search.state;
				$scope.get_cities();
			}
		});
	}


  $scope.get_cities = function() {	  
	  $scope.get_properties();
		$http.get( '/cities/by_state/' + $scope.search.state ).success( function( cities ) {
			$scope.cities = cities;
			if ( search.city ) {
				$scope.search["city"] = search.city;
				$scope.get_districts();
			}
		});
	}

  $scope.get_districts = function() {
		$scope.get_properties();
		$http.get( '/districts/by_city/' + $scope.search.city ).success( function( districts ) {
			$scope.districts = districts;
			if ( search.district ) {
				$scope.search["district"] = search.district;
			}
		});
	}

	$scope.set_utilization = function() {
		$scope.get_properties();
		if ( search.utilization ) {
			$scope.search["utilization"] = search.utilization;
			$scope.utilization_change();
		}
	}

	$scope.utilization_change = function() {
		$scope.get_properties();
    $scope.search.options_type = propertyKind( $scope.search.utilization );
		if ( search.type ) {
			$scope.search["property_type"] = search.type;
		}
  }

	$scope.get_states();
	$scope.set_utilization();
	//$scope.get_properties();
}

realtyExchangeApp.controller('PropertiesResultsCtrl', ['$scope', '$http', '$location', 
		'$routeParams', 'propertyKind', 'getMainPhoto', PropertiesResultsCtrl]);
