function PropertyPhotosCtrl($scope, $http, $routeParams, $location, uiUploader) {
  $scope.params = $routeParams;
  console.log( $scope.params.id );

  $scope.photos = [];

  $scope.get_property = function() {
    $http.get( '/properties/photos/' + $scope.params.id )
      .success( function( photos ) {
			  $scope.photos = photos;
        console.log( photos );
		  });
  };

  $scope.finalize = function() {
    $location.path( "/dashboard" );
  }

  $scope.btn_remove = function(file) {
    console.log('deleting=' + file);
    uiUploader.removeFile(file);
  };

  $scope.btn_clean = function() {
    uiUploader.removeAll();
  };

  $scope.btn_upload = function() {
    console.log('uploading...');
    uiUploader.startUpload({
      url: '/properties/photo/' + $scope.params.id,
      concurrency: 2,
      onProgress: function(file) {
        console.log(file.name + '=' + file.humanSize);
        $scope.$apply();
      },
      onCompleted: function(file, response) {
        console.log(file + 'response' + response);
      },
      headers: {
        'X-CSRF-TOKEN' : document.querySelector('meta[name="csrf-token"]').getAttribute('content')
      }
    });
  };

  $scope.files = [];
  var element = document.getElementById('file1');
  element.addEventListener('change', function(e) {
    var files = e.target.files;
    uiUploader.addFiles(files);
    $scope.files = uiUploader.getFiles();
    console.log( $scope.files );
    $scope.$apply();
  });

  $scope.get_property();

}

realtyExchangeApp.controller('PropertyPhotosCtrl', ['$scope', '$http', "$routeParams", "$location", "uiUploader", PropertyPhotosCtrl]);
