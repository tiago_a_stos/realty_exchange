function UserDashboardCtrl($scope, $http, getMainPhoto) {
	$http.get( '/user/signed_in' )
		.success( function( data ) {
			console.log( "Logado ");
		}). error( function( data ) {
			window.location = '/';
		})

	$scope.get_properties = function() {
		$http.get( '/properties/my' ).success( function( properties ) {
			$scope.properties = getMainPhoto( properties );
		});
	};

	$scope.get_properties_intended = function() {
		$http.get( '/properties_intended/my' ).success( function( properties_intended ) {
			$scope.properties_intended = properties_intended;
		});
	};

	$scope.get_properties();
	$scope.get_properties_intended();
}

realtyExchangeApp.controller('UserDashboardCtrl', ['$scope', '$http', 'getMainPhoto', UserDashboardCtrl]);
