function LoginCtrl($scope, $http) {
  $scope.login = function() {
		console.log( $scope.user );
		var user = { "user": $scope.user }

		$scope.login_message = "";

		$http.post( "/users/sign_in/", user )
			.success( function( user ) {
				window.location = "/";
				//$location.path( user.location );
			})
			.error( function( data ) {
				console.log( "Não Logado" );
				console.log( data );
				$scope.login_message = data.message;
			});
	}
}

realtyExchangeApp.controller('LoginCtrl', ['$scope', '$http', LoginCtrl]);
