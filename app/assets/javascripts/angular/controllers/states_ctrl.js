function StatesCtrl($scope, $http) {
	$http.get( '/user/signed_in' )
		.success( function( data ) {
		})
		.error( function( data ) {
			console.log( "Não Logado" );
			window.location = '/';
		})


	$scope.save = function() {
		var state = { "state": $scope.state }

		$http.post( "/state/", state )
			.success( function( user ) {
				$scope.get_states();
			})
			.error( function( data ) {
				console.log( "Não Logado" );
				console.log( data );
				$scope.login_message = data.message;
			});
	}

	$scope.get_states = function() {
		$http.get( '/states/all' ).success( function( states ) {
			$scope.states = states;
		});
	}

	$scope.get_states();
}

realtyExchangeApp.controller('StatesCtrl', ['$scope', '$http', StatesCtrl]);
