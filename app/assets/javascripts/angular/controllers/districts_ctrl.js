function DistrictsCtrl($scope, $http) {
	$http.get( '/user/signed_in' )
		.success( function( data ) {
		})
		.error( function( data ) {
			console.log( "Não Logado" );
			window.location = '/';
		})

	$scope.get_districts = function() {
		$http.get( '/districts/all' ).success( function( districts ) {
			console.log( districts);
			$scope.districts = districts;
		});
	}

	$scope.save = function() {
		var district = { "district": $scope.district }

		$http.post( "/district/", district )
			.success( function( district ) {
				$scope.get_districts();
			})
			.error( function( data ) {
				console.log( data );
				$scope.login_message = data.message;
			});
	}

	$scope.get_states = function() {
		$http.get( '/states/all' ).success( function( states ) {
			$scope.states = states;
		});
	}

	$scope.get_cities = function() {
		$http.get( '/cities/by_state/' + $scope.state ).success( function( cities ) {
			$scope.cities = cities;
		});
	}

	$scope.get_districts();
	$scope.get_states();
}

realtyExchangeApp.controller('DistrictsCtrl', ['$scope', '$http', DistrictsCtrl]);
