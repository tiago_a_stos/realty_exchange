function HomeCtrl($scope, $http, $location, propertyKind, getMainPhoto) {
	$(".navigation li").removeClass("active");
	$("#menu-dashboard").addClass("active");
  
  $scope.properties = [];

	$scope.get_properties = function() {
		$http.get( '/properties/all' ).success( function( properties ) {
      $scope.properties = getMainPhoto( properties );
		});
	};

	$scope.get_properties();

  $scope.utilization_change = function() {
    console.log( $scope.search.utilization);
    $scope.search.options_type = propertyKind( $scope.search.utilization );
  }

  $scope.get_states = function() {
		$http.get( '/states/all' ).success( function( states ) {
			$scope.states = states;
		});
	}

	$scope.get_states();

  $scope.get_cities = function() {
		$http.get( '/cities/by_state/' + $scope.search.state ).success( function( cities ) {
			$scope.cities = cities;
		});
	}

  $scope.get_districts = function() {
		$http.get( '/districts/by_city/' + $scope.search.city ).success( function( districts ) {
			$scope.districts = districts;
		});
	}

  $scope.search = function() {
    search_params = $scope.search;
    //console.log(search_params);

    var url = "/search?state=" +( search_params.state || "") + "&city=" + ( search_params.city || "" ) + "&district=" + ( search_params.district || "" ) + "&utilization=" + ( search_params.utilization || "" ) + "&type=" + ( search_params.property_type || "" );
    $location.url( url );
  }

  /**
   * Configuração para slides
  **/
	$scope.myInterval = 5000;
  $scope.noWrapSlides = false;
  $scope.active = 0;
  var slides = $scope.slides = [];
  var currIndex = 0;

  $scope.addSlide = function() {
    var newWidth = 600 + slides.length + 1;
    slides.push({
      image: '//unsplash.it/' + newWidth + '/300',
      text: ['Nice image','Awesome photograph','That is so cool','I love that'][slides.length % 4],
      id: currIndex++
    });
  };

  $scope.randomize = function() {
    var indexes = generateIndexesArray();
    assignNewIndexesToSlides(indexes);
  };

  for (var i = 0; i < 4; i++) {
    console.log("Adicionando slide");
    $scope.addSlide();
  }

  // Randomize logic below

  function assignNewIndexesToSlides(indexes) {
    for (var i = 0, l = slides.length; i < l; i++) {
      slides[i].id = indexes.pop();
    }
  }

  function generateIndexesArray() {
    var indexes = [];
    for (var i = 0; i < currIndex; ++i) {
      indexes[i] = i;
    }
    return shuffle(indexes);
  }

  // http://stackoverflow.com/questions/962802#962890
  function shuffle(array) {
    var tmp, current, top = array.length;

    if (top) {
      while (--top) {
        current = Math.floor(Math.random() * (top + 1));
        tmp = array[current];
        array[current] = array[top];
        array[top] = tmp;
      }
    }

    return array;
  }
}

realtyExchangeApp.controller('HomeCtrl', ['$scope', '$http', '$location', 'propertyKind', 
    'getMainPhoto', HomeCtrl]);
