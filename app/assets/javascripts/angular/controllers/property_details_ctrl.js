function PropertyDetailsCtrl($scope, $http, $routeParams) {
  $scope.params = $routeParams;
  console.log( $scope.params.id );

  $scope.get_property = function() {
    $http.get( '/property/' + $scope.params.id )
      .success( function( property ) {
			  $scope.property = property;
        console.log("=================================================================");
        console.log( property);
        console.log("=================================================================");
		  });
  }

  $scope.get_property();
}

realtyExchangeApp.controller('PropertyDetailsCtrl', ['$scope', '$http', "$routeParams", PropertyDetailsCtrl]);
