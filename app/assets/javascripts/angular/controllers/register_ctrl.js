function RegisterCtrl($scope, $http) {

	/**
	 * CAMPOS MASCARADODS
	 */
	VMasker( $('#register-cpf') ).maskPattern("999.999.999-99");
	VMasker( $('.phone-mask') ).maskPattern("(99) 9999-9999");
	VMasker( $('.cell-phone-mask') ).maskPattern("(99) 99999-9999");
	VMasker( $(".number-mask") ).maskNumber();

  $scope.register = function() {
		if (!$scope.validate()) {
			return;
		}

		var user = { "registration": $scope.user }

		$scope.login_message = "";

		$http.post( "/users/", user )
			.success( function( data ) {
				console.log(data);
				if (data.success == false) {
					console.log( data.message );
					$scope.login_message = data.message;

				} else {
					window.location = "/";
					//$location.path( user.location );
				}
			})
			.error( function( data ) {
				console.log( data );
				$scope.login_message = data.message;
			});
	}

	$scope.validate = function() {
		$scope.name_ni = false;
		$scope.last_name_ni = false;
		$scope.gender_ni = false;
		$scope.rg_ni = false;
		$scope.cpf_ni = false;
		$scope.marital_state_ni = false;
		$scope.home_phone_ni = false;
		$scope.cell_phone_ni = false;

		$scope.email_ni = false;
		$scope.password_ni = false;
		$scope.password_confirmation_ni = false;

		validated = true;

		if ( $scope.user == null ) {
			$scope.name_ni = true;
			$scope.last_name_ni = true;
			$scope.gender_ni = true;
			$scope.rg_ni = true;
			$scope.cpf_ni = true;
			$scope.marital_status_ni = true;
			$scope.home_phone_ni = true;
			$scope.cell_phone_ni = true;

			$scope.email_ni = true;
			$scope.password_ni = true;
			$scope.password_confirmation_ni = true;
			validated = false;

		} else {
			if ( $scope.user.name == null || $scope.user.name == "" ) {
				$scope.name_ni = true;
				validated = false;
			}

			if ( $scope.user.last_name == null || $scope.user.last_name == "" ) {
				$scope.last_name_ni = true;
				validated = false;
			}

			if ( $scope.user.gender == null || $scope.user.gender == "" ) {
				$scope.gender_ni = true;
				validated = false;
			}

			if ( $scope.user.rg == null || $scope.user.rg == "" ) {
				$scope.rg_ni = true;
				validated = false;
			}


			if ( $scope.user.cpf == null || $scope.user.cpf == "" ) {
				$scope.cpf_ni = true;
				validated = false;
			}

			if ( $scope.user.marital_status == null || $scope.user.marital_status == "" ) {
				$scope.marital_status_ni = true;
				validated = false;
			}

			if ( $scope.user.home_phone == null || $scope.user.home_phone == "" ) {
				$scope.home_phone_ni = true;
				validated = false;
			}

			if ( $scope.user.cell_phone == null || $scope.user.cell_phone == "" ) {
				$scope.cell_phone_ni = true;
				validated = false;
			}

			if ( $scope.user.email == null || $scope.user.email == "" ) {
				$scope.email_ni = true;
				validated = false;
			}

			if ( $scope.user.password == null || $scope.user.password == "" ) {
				$scope.password_ni = true;
				validated = false;
			}

			if ( $scope.user.password_confirmation == null || $scope.user.password_confirmation == "" ) {
				$scope.password_confirmation_ni = true;
				validated = false;
			}
		}

		return validated;
	};
}

realtyExchangeApp.controller('RegisterCtrl', ['$scope', '$http', RegisterCtrl]);
