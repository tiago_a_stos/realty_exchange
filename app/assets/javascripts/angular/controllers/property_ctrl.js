function PropertyCtrl($scope, $http, $location, propertyKind) {
	$scope.property = {};

	$http.get( '/user/signed_in' )
		.success( function( data ) {
			console.log( "Logado ");
		})
		.error( function( data ) {
			window.location = '/';
		});


	/**
	 * CAMPOS MASCARADODS
	 */
	VMasker( $('#location_cep') ).maskPattern("99999-999");
	VMasker( $('#building_year') ).maskPattern("9999");
	VMasker( $("[type|='number']") ).maskNumber();
	VMasker( $("#value_sale") ).maskMoney();
	VMasker( $("#value_condo") ).maskMoney();
	VMasker( $("#value_tax") ).maskMoney();

	$scope.selected_unit_details = {};
	$scope.unit_details = [
	    {key: "academy", value: "Academia/fitness", utilizations: ["RESIDENCIAL"]},
	    {key: "acccess_24h", value: "Acesso 24 Horas", utilizations: ["RESIDENCIAL", "COMERCIAL", "INDUSTRIAL"]},
	    {key: "alarm", value: "Alarme", utilizations: ["RESIDENCIAL", "COMERCIAL", "INDUSTRIAL", "RURAL"]},
	    {key: "heater", value: "Aquecedor", utilizations: ["RESIDENCIAL", "RURAL"]},
	    {key: "heating", value: "Aquecimento", utilizations: ["RESIDENCIAL", "RURAL"]},
	    {key: "air_conditioning", value: "Ar Condicionado", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	    {key: "kitchen_cabinet", value: "Arm.cozinha", utilizations: ["RESIDENCIAL", "RURAL"]},
	    {key: "built_in_cupboard", value: "Armário Embutido", utilizations: ["RESIDENCIAL", "RURAL"]},
	    {key: "bathtub", value: "Banheira", utilizations: ["RESIDENCIAL", "RURAL"]},
	    {key: "bar", value: "Bar", utilizations: ["RESIDENCIAL", "RURAL"]},
	    {key: "library", value: "Biblioteca", utilizations: ["RESIDENCIAL", "RURAL"]},
	    {key: "business_center", value: "Business Center", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	    {key: "carpet", value: "Carpete", utilizations: ["RESIDENCIAL", "RURAL"]},
	    {key: "caretaker_house", value: "Casa de Caseiro", utilizations: ["RESIDENCIAL", "INDUSTRIAL", "RURAL"]},
	    {key: "back_house", value: "Casa de Fundo", utilizations: ["RESIDENCIAL", "INDUSTRIAL", "RURAL"]},
	    {key: "fence", value: "Cerca", utilizations: ["RESIDENCIAL", "COMERCIAL", "INDUSTRIAL", "RURAL"]},
	    {key: "barbecue_grill", value: "Churrasqueira", utilizations: ["RESIDENCIAL", "RURAL"]},
	    {key: "tv_circuit", value: "Circuíto de Televisão", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	    {key: "closet", value: "Closet", utilizations: ["RESIDENCIAL", "RURAL"]},
	    {key: "cup", value: "Copa", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	    {key: "kitchen", value: "Cozinha", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	    {key: "american_kitchen", value: "Cozinha Americana", utilizations: ["RESIDENCIAL", "RURAL"]},
	    {key: "gourmet_kitchen", value: "Cozinha Gourmet", utilizations: ["RESIDENCIAL", "RURAL"]},
	    {key: "independent_kitchen", value: "Cozinha Independente", utilizations: ["RESIDENCIAL", "RURAL"]},
	    {key: "employee_dependency", value: "Dependência de Empregados", utilizations: ["RESIDENCIAL", "INDUSTRIAL", "RURAL"]},
	    {key: "deposit", value: "Deposito", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	    {key: "underground_deposit", value: "Deposito Subsolo", utilizations: ["RESIDENCIAL", "RURAL"]},
	    {key: "private_deposit", value: "Depósito Privativo", utilizations: ["RESIDENCIAL", "RURAL"]},
	    {key: "pantry", value: "Despensa", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	    {key: "shower", value: "Ducha", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	    {key: "office", value: "Escritorio", utilizations: ["RESIDENCIAL", "INDUSTRIAL", "RURAL"]},
	    {key: "sewer", value: "Esgoto", utilizations: ["RESIDENCIAL", "INDUSTRIAL", "RURAL"]},
	    {key: "eletric_stove", value: "Fogao Eletrico", utilizations: ["RESIDENCIAL", "RURAL"]},
	    {key: "freezer", value: "Freezer", utilizations: ["RESIDENCIAL", "RURAL"]},
	    {key: "sea_front", value: "Frente para o Mar", utilizations: ["RESIDENCIAL", "COMERCIAL"]},
	    {key: "refrigerator", value: "Geladeira", utilizations: ["RESIDENCIAL"]},
	    {key: "twinned", value: "Geminada", utilizations: ["RESIDENCIAL"]},
	    {key: "piped_gas", value: "Gás Encanado", utilizations: ["RESIDENCIAL", "INDUSTRIAL", "RURAL"]},
	    {key: "natural_gas", value: "Gás Natural", utilizations: ["RESIDENCIAL", "INDUSTRIAL", "RURAL"]},
	    {key: "whirlpool", value: "Hidromassagem", utilizations: ["RESIDENCIAL"]},
	    {key: "home_theater", value: "Home Theater", utilizations: ["RESIDENCIAL", "RURAL"]},
	    {key: "intercom", value: "Interfone", utilizations: ["RESIDENCIAL", "COMERCIAL", "INDUSTRIAL", "RURAL"]},
	    {key: "wifi", value: "Internet Wireless", utilizations: ["RESIDENCIAL", "COMERCIAL"]},
	    {key: "soundproofing", value: "Isolamento Acústico", utilizations: ["RESIDENCIAL", "COMERCIAL"]},
	    {key: "jacuzzi", value: "Jacuzzi", utilizations: ["RESIDENCIAL", "RURAL"]},
	    {key: "fireplace", value: "Lareira", utilizations: ["RESIDENCIAL", "RURAL"]},
	    {key: "washbasin", value: "Lavabo", utilizations: ["RESIDENCIAL", "RURAL"]},
	    {key: "chandelier", value: "Lustres", utilizations: ["RESIDENCIAL", "COMERCIAL", "INDUSTRIAL", "RURAL"]},
	    {key: "mezzanine", value: "Mezanino", utilizations: ["RESIDENCIAL", "COMERCIAL", "INDUSTRIAL", "RURAL"]},
	    {key: "furniture", value: "Mobilia", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	    {key: "pool", value: "Piscina", utilizations: ["RESIDENCIAL", "RURAL"]},
	    {key: "wood_floor", value: "Piso de Madeira", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	    {key: "raised_floor", value: "Piso Elevado", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	    {key: "cold_floor", value: "Piso Frio", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	    {key: "laminate_floor", value: "Piso Laminado", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	    {key: "basement", value: "Porão", utilizations: ["RESIDENCIAL", "RURAL"]},
	    {key: "employees_room", value: "Quarto Empregados", utilizations: ["RESIDENCIAL", "RURAL"]},
	    {key: "backyard", value: "Quintal", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	    {key: "balcony", value: "Sacada", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	    {key: "living_room", value: "Sala de Estar", utilizations: ["RESIDENCIAL", "RURAL"]},
	    {key: "dining_room", value: "Sala de Jantar", utilizations: ["RESIDENCIAL", "RURAL"]},
	    {key: "sauna", value: "Sauna", utilizations: ["RESIDENCIAL", "RURAL"]},
	    {key: "street_safety", value: "Segurança Na Rua", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	    {key: "semi_furnished", value: "Semi Mobiliado", utilizations: ["RESIDENCIAL", "RURAL"]},
	    {key: "attic", value: "Sotão", utilizations: ["RESIDENCIAL", "RURAL"]},
	    {key: "loft", value: "Superior Residência e Inferior", utilizations: ["RESIDENCIAL", "RURAL"]},
	    {key: "terrace", value: "Terraco", utilizations: ["RESIDENCIAL", "RURAL"]},
	    {key: "lowered_ceiling", value: "Teto Rebaixado", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	    {key: "porch", value: "Varanda", utilizations: ["RESIDENCIAL", "RURAL"]},
	    {key: "porch_with_barbecue", value: "Varanda com Churrasqueira", utilizations: ["RESIDENCIAL", "RURAL"]},
	    {key: "enclosed_porch_with_glass", value: "Varanda Fechada com Vidro", utilizations: ["RESIDENCIAL", "RURAL"]},
	    {key: "gourmet_porch", value: "Varanda Gourmet", utilizations: ["RESIDENCIAL", "RURAL"]},
	    {key: "integrated_porch_with_kitchen", value: "Varanda Integrada com a Cozinh", utilizations: ["RESIDENCIAL", "RURAL"]},
	    {key: "employee_bathroom", value: "Wc Empregados", utilizations: ["RESIDENCIAL", "INDUSTRIAL"]}
	  ];

	$scope.selected_enterprise_details = {};
	$scope.enterprise_details = [
	  {key: "academy", value: "Academia", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "access_to_railway", value: "Acesso a Linha Ferrea", utilizations: ["COMERCIAL", "RURAL"]},
	  {key: "access_physically_handicapped", value: "Acesso P/ Deficiente Físico", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "wine_house", value: "Adega", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "administrator", value: "Administradora", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "warehouse", value: "Almoxarifado", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "central_air_conditioning", value: "Ar Condicionado Central", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "air_conditioning_split", value: "Ar Condicionado Split", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "outdoor_seating_area", value: "Área de Estar Externa", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "recreation_area", value: "Area de Lazer", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "service_area", value: "Área de Serviço", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "green_area", value: "Area Verde", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "atelier", value: "Atelier", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "auditorium", value: "Auditorio", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "baby_child_care", value: "Baby Child Care", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "babe_place_with_changing_room", value: "Baby Place com Fraldario", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "baby_space", value: "Baby Space", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "banbolo", value: "Bangolo", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "bar", value: "Bar", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "acapulco_bar", value: "Bar Acapulco", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "wet_bar", value: "Bar Molhado", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "library", value: "Biblioteca", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "bicycle_rack", value: "Bicicletario", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "bike_indoor", value: "Bike Indoor", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "biribol", value: "Biribol", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "body_center", value: "Body Center", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "bowling", value: "Boliche", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "forest", value: "Bosque", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "boxing", value: "Boxe", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "bricolage", value: "Bricolage", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "toy_library", value: "Brinquedoteca", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "bamboo_path", value: "Caminho Dos Bambus", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "football_field", value: "Campo de Futebol", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "golf_course", value: "Campo de Golfe", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "dog_kennel", value: "Canil", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "car_wash", value: "Car Wash", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "tarzan_house", value: "Casa do Tarzan", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "house_made", value: "Caseiro", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "telephone_switchboard", value: "Central Telefônica", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "central_of_aesthetics", value: "Centro de Estetica", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "electric_fence", value: "Cerca Elétrica", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "child_care", value: "Child Care", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "barbecue_grill", value: "Churrasqueira", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "gourmet_barbecue", value: "Churrasqueira Gourmet", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "movie_theater", value: "Cinema", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "skylight", value: "Clarabóia", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "club", value: "Clube", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "coffee_shop", value: "Coffee Shop", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "american_kitchen", value: "Cozinha Americana", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "gourmet_kitchen", value: "Cozinha Gourmet", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "cyber_room", value: "Cyber Room", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "wet_deck", value: "Deck Molhado", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "rest", value: "Descanso", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "disc", value: "Disco", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "shower", value: "Ducha", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "elevator", value: "Elevador", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "elevator_with_password", value: "Elevador com Senha", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "private_elevator", value: "Elevador Privativo", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "electricity", value: "Energia Elétrica", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "truck_entrance", value: "Entrada de Caminhões", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "service_entrance", value: "Entrada de Serviço Independent", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "facilitated_entrance", value: "Entrada Facilitada", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "side_entry", value: "Entrada Lateral", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "virtual_office", value: "Escritório Virtual", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "circus_space", value: "Espaco Circo", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "cultural_space", value: "Espaco Cultural", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "gourmet_space", value: "Espaco Gourmet", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "women_space", value: "Espaco Mulher", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "women_space_with_massage", value: "Espaco Mulher com Sala de Mass", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "zen_space", value: "Espaco Zen", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "spiribel", value: "Espiribol", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "parking", value: "Estacionamento", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "covered_parking", value: "Estacionamento Coberto", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "visitor_parking", value: "Estacionamento para Visitantes", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "short_term_parking", value: "Estacionamento Rotativo", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "living_room_with_pergola", value: "Estar com Pergolado Junto a Pi", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "living_room_next_leisure_room", value: "Estar Junto ao Lazer Interno", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "living_room_in_the_water", value: "Estar Na Agua", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "paved_street", value: "Estrada/rua Asfaltada", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "fitness", value: "Fitness", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "outdoor_fitness", value: "Fitness ao Ar Livre", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "wood_oven", value: "Forno a Lenha", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "pizza_oven", value: "Forno de Pizza", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "lining", value: "Forro", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "game_station", value: "Game Station", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "garage_band", value: "Garage Band", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "garage", value: "Garagem", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "covered_garage", value: "Garagem Coberta", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "uncovered_garage", value: "Garagem Descoberta", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "independent_garage", value: "Garagem Independente", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "reading_gazebo", value: "Gazebo de Leitura", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "generator", value: "Gerador", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "security_cabin", value: "Guarita", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "hall", value: "Hall", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "heliport", value: "Heliponto", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "whirlpool", value: "Hidromassagem", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "home_office", value: "Home Office", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "garden", value: "Horta", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "infra_internet", value: "Infra-Internet", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "internet", value: "Internet", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "shared_internet", value: "Internet Compartilhada", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "yard", value: "Jardim", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "winter_yard", value: "Jardim de Inverno", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "ornamental_yard", value: "Jardim Ornamental", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "lake", value: "Lago", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "lan_house", value: "Lan House", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "laundry", value: "Lavanderia", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "collective_laundry", value: "Lavanderia Coletiva", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "double_height_lobby", value: "Lobby com Pe Direito Duplo", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "lounge", value: "Lounge", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "lounge_with_fireplace", value: "Lounge com Lareira", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "marina", value: "Marina", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "massage", value: "Massagem", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "mini_tree_climbing", value: "Mini Arvorismo", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "music_lounge", value: "Music Lounge", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "workshop", value: "Oficina", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "ofuro", value: "Ofuro", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "orchid", value: "Orquidario", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "park", value: "Parque", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "courtyard_parking", value: "Pátio Estacionamento", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "pergola", value: "Pergolado", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "pets_allowed", value: "Permite Animais", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "personal_training", value: "Personal Training", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "fishing", value: "Pesqueiro", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "pet_care", value: "Pet Care", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "pet_place", value: "Pet Place", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "pet_play", value: "Pet Play", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "piano_bar", value: "Piano Bar", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "pier", value: "Píer", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "pool", value: "Piscina", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "adult_pool", value: "Piscina Adulto", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "heated_pool", value: "Piscina Aquecida", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "acclimatized_pool", value: "Piscina Climatizada", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "indoor_poll", value: "Piscina Coberta", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "indoor_acclimatized_pool", value: "Piscina Coberta Climatizada", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "indoor_lap_pool", value: "Piscina Coberta com Raia", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "pool_with_hidromassage", value: "Piscina com Hidromassagem", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "lap_pool", value: "Piscina com Raia", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "childrens_pool", value: "Piscina Infantil", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "ozonized_pool", value: "Piscina Ozonizada", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "lap_pool_25m", value: "Piscina Raia 25M", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "semi_covered_pool", value: "Piscina Semi Coberta", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "cooper_track", value: "Pista Cooper", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "athletics_pool", value: "Pista de Atletismo", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "bike_lane", value: "Pista de Bicicross", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "walking_track", value: "Pista de Caminhada", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "cooper_track_with_equipment", value: "Pista de Cooper com Aparelhos", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "skating_rink", value: "Pista de Patinacao", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "landing_track", value: "Pista de Pouso", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "roller_skate_track", value: "Pista de Roller Skate", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "skate_track", value: "Pista Skate", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "playground", value: "Playground", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "baby_playground", value: "Playground Baby", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "junior_playground", value: "Playground Junior", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "well", value: "Poço", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "artesian_well", value: "Poço Artesiano", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "orchard", value: "Pomar", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "port_cochere", value: "Port Cochere", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "automatic_gate", value: "Portao Automatico", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "concierge_12h", value: "Portaria 12Hrs", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "concierge_24h", value: "Portaria 24Hrs", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "ticket_gate", value: "Portaria/catraca", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "bank_post", value: "Posto Bancario", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "jaboticaba_square", value: "Praca da Jabuticaba", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "water_square", value: "Praca das Aguas", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "leisure_square", value: "Praca de Lazer", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "aromas_square", value: "Praca Dos Aromas", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "squere", value: "Pracas", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "beach", value: "Prainha", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "near_subway", value: "Próximo ao Metrô", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "pub", value: "Pub", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "sports_court", value: "Quadra de Esportes", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "futsal_court", value: "Quadra de Futsal", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "shuttlecock_court", value: "Quadra de Peteca", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "squash_court", value: "Quadra de Squash", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "tennis_court", value: "Quadra de Tenis", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "beach_volleyball_court", value: "Quadra de Volei de Praia", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "grass_court", value: "Quadra Gramada", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "sportsware_court", value: "Quadra Poliesportiva", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "kiosk_with_barbecue", value: "Quiosque com Churrasqueira e F", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "reception", value: "Recepção", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "reception_24h", value: "Recepção 24 Horas", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "hammocks", value: "Redario", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "telephone_network", value: "Rede Telefonica", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "refectory", value: "Refeitório", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "water_tank", value: "Reservatório de Água", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "restaurant", value: "Restaurante", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "river", value: "Rio", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "emergency_exit", value: "Saída Emergência", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "lunch_room", value: "Sala de Almoço", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "conference_room", value: "Sala de Conferência", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "convention_room", value: "Sala de Convencoes", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "study_room", value: "Sala de Estudo", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "gym_room", value: "Sala de Ginastica", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "reading_room", value: "Sala de Leitura", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "massage_room", value: "Sala de Massagem", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "meeting_room", value: "Sala de Reunioes", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "tv_room", value: "Sala de Tv", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "yoga_room", value: "Sala de Yoga", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "intimate_room", value: "Sala Íntima", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "beauty_salon", value: "Salao de Beleza", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "party_hall", value: "Salao de Festa", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "teen_party_hall", value: "Salao de Festas Adolecente e J", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "adult_party_hall", value: "Salao de Festas Adulto", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "children_party_hall", value: "Salao de Festas Infantil", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "games_room", value: "Salao de Jogos", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "adult_games_room", value: "Salao de Jogos Adulto", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "games_room_with_lounge", value: "Salao de Jogos com Lounge Juve", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "infantil_game_room", value: "Salão de Jogos Infantil", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "gourmet_salon", value: "Salao Gourmet", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "sauna", value: "Sauna", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "sauna_with_rest_room", value: "Sauna com Sala de Descanso", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "sauna_and_shower", value: "Sauna e Ducha", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "dry_sauna", value: "Sauna Seca", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "wet_saunda", value: "Sauna Umida", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "headquarters", value: "Sede", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "safety", value: "Segurança", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "safety_24h", value: "Segurança 24 Horas", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "property_security", value: "Segurança Patrimonial", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "semigeminated", value: "Semigeminada", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "services", value: "Serviços", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "office_services", value: "Serviços de Escritório", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "golf_simulator", value: "Simulador de Golf", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "fire_system", value: "Sistema de Incendio", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "security_system", value: "Sistema de Segurança", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "alarm_system", value: "Sistemas de Alarme", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "solarium", value: "Solarium", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "spa", value: "Spa", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "spa_sauna_ofuro", value: "Spa com Sauna e Ofuro", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "fire_sprinklers", value: "Sprinklers Contra Incêndio", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "tecno_lounge", value: "Tecno Lounge", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "phone", value: "Telefone", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "television", value: "Televisão", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "cable_tv", value: "Televisão a Cabo", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "reading_tent", value: "Tenda da Leitura", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "gourmet_terrace", value: "Terraço Gourmet", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "waterslide", value: "Toboagua", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "toboggan", value: "Toboga", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "trail_in_the_woods", value: "Trilha Na Mata com Redario", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "ceiling_fan", value: "Ventilador de Teto", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "employees_dressing_room", value: "Vestiário para Empregados", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "dressing_room", value: "Vestiarios", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "vigilance_24h", value: "Vigilância 24 Horas", utilizations: ["RESIDENCIAL", "COMERCIAL", "RURAL"]},
	  {key: "view_of_the_sea", value: "Vista para o Mar", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "web_space", value: "Web Space", utilizations: ["RESIDENCIAL", "RURAL"]},
	  {key: "caretaker", value: "Zelador", utilizations: ["RESIDENCIAL", "RURAL"]}
	];


	$scope.exchange_option_change = function() {
		if ($scope.study_exchange == true) {
			$("#study_exchange_form").removeClass("hide");
		} else {
			$("#study_exchange_form").addClass("hide");
		}
	};

	/**
	 * SAVING THE PROPERTY
	 */
	$scope.save = function() {

		if (!$scope.validate()) {
			return;
		}

		details = {};

		var selected_unit = {};
		for ( item in $scope.selected_unit_details ) {
			if ( $scope.selected_unit_details[item] == false ) {
				delete $scope.selected_unit_details[item];
			}
		}

		details["unit"] = $scope.selected_unit_details;

		var selected_enterp = {};
		for ( item in $scope.selected_enterprise_details ) {
			if ( $scope.selected_enterprise_details[item] == false ) {
				delete $scope.selected_enterprise_details[item];
			}
		}

		details["enterprise"] = $scope.selected_enterprise_details;
		$scope.property["details"] = details;
		var property = {property: $scope.property};

		$http.post('/property', property)
			.success( function( data ) {
				console.log( data );
				$location.path( "/property-photos/" + data.id );
			})
			.error( function( data ) {
				console.log( data );
			});
	}

	$scope.validate = function() {
		$scope.utilization_not_selected = false;
		$scope.type_not_selected = false;
		$scope.cep_not_informed = false;
		$scope.street_ni = false;
		$scope.number_ni = false;
		$scope.district_ni = false;
		$scope.city_ni = false;
		$scope.state_ni = false;

		$scope.face_ni = false;
		$scope.floors_number_ni = false;
		$scope.units_per_floor_ni = false;
		$scope.living_room_number_ni = false;
		$scope.bathroom_number_ni = false;
		$scope.parking_spaces_ni = false;
		$scope.suites_number_ni = false;
		$scope.dormitories_number_ni = false;

		$scope.sale_value_ni = false;
		$scope.condo_value_ni = false;
		$scope.tax_value_ni = false;

		validated = true;

		if ( $scope.property.kind == null ) {
			$scope.utilization_not_selected = true;
			$scope.type_not_selected = true;
			validated = false;

		} else {
			if ( $scope.property.kind.utilization == null || $scope.property.kind.utilization == "" ) {
				$scope.utilization_not_selected = true;
				validated = false;
			}

			if ( $scope.property.kind.type == null || $scope.property.kind.type == "" ) {
				$scope.type_not_selected = true;
				validated = false;
			}
		}

		if ( $scope.property.location == null ) {
			$scope.cep_not_informed = true;
			validated = false;

		} else {
			if ( $scope.property.location.postal_code == null ) {
				$scope.cep_not_informed = true;
				validated = false;
			}

			if ( $scope.property.location.street == null ) {
				$scope.street_ni = true;
				validated = false;
			}

			if ( $scope.property.location.number == null ) {
				$scope.number_ni = true;
				validated = false;
			}

			if ( $scope.property.location.district == null ) {
				$scope.district_ni = true;
				validated = false;
			}

			if ( $scope.property.location.city == null ) {
				$scope.city_ni = true;
				validated = false;
			}

			if ( $scope.property.location.state == null ) {
				$scope.state_ni = true;
				validated = false;
			}
		}

		if ( $scope.property.caracteristics == null ) {
			if ( $scope.disable_dormitories ) {
				$scope.dormitories_number_ni = false;
			} else {
				$scope.dormitories_number_ni = true;
			}
			if ( $scope.disable_suites ) {
				$scope.suites_number_ni = false;
			} else {
				$scope.suites_number_ni = true;
			}
			if ( $scope.disable_units_per_floor ) {
				$scope.units_per_floor_ni = false;
			} else {
				$scope.units_per_floor_ni = true;
			}
			if ( $scope.disable_floors_number ) {
				$scope.floors_number_ni = false;
			} else {
				$scope.floors_number_ni = true;
			}
			if ( $scope.disable_face ) {
				$scope.face_ni = false;
			} else {
				$scope.face_ni = true;
			}

			$scope.living_room_number_ni = true;
			$scope.bathroom_number_ni = true;
			$scope.parking_spaces_ni = true;
			validated = false;

		} else {
			if ( $scope.property.caracteristics.dormitories_number == null ) {
				if ( $scope.disable_dormitories ) {
					$scope.dormitories_number_ni = false;
				} else {
					$scope.dormitories_number_ni = true;
					validated = false;
				}
			}

			if ( $scope.property.caracteristics.suites_number == null ) {
				if ( $scope.disable_suites ) {
					$scope.suites_number_ni = false;
				} else {
					$scope.suites_number_ni = true;
					validated = false;
				}
			}

			if ( $scope.property.caracteristics.parking_spaces == null ) {
				$scope.parking_spaces_ni = true;
				validated = false;
			}

			if ( $scope.property.caracteristics.bathroom_number == null ) {
				$scope.bathroom_number_ni = true;
				validated = false;
			}

			if ( $scope.property.caracteristics.living_room_number == null ) {
				$scope.living_room_number_ni = true;
				validated = false;
			}

			if ( $scope.property.caracteristics.units_per_floor == null ) {
				if ( $scope.disable_units_per_floor ) {
					$scope.units_per_floor_ni = false;
				} else {
					$scope.units_per_floor_ni = true;
					validated = false;
				}
			}

			if ( $scope.property.caracteristics.floors_number == null ) {
				if ( $scope.disable_floors_number ) {
					$scope.floors_number_ni = false;
				} else {
					$scope.floors_number_ni = true;
					validated = false;
				}
			}

			if ( $scope.property.caracteristics.face == null ) {
				if ( $scope.disable_face ) {
					$scope.face_ni = false;
				} else {
					$scope.face_ni = true;
					validated = false;
				}
			}
		}

		if ( $scope.property.values == null ) {
			$scope.sale_value_ni = true;
			$scope.condo_value_ni = true;
			$scope.tax_value_ni = true;
			validated = false;

		} else {
			if ( $scope.property.values.sale == null ) {
				$scope.sale_value_ni = true;
				validated = false;
			}

			if ( $scope.property.values.condo == null ) {
				$scope.condo_value_ni = true;
				validated = false;
			}

			if ( $scope.property.values.tax == null ) {
				$scope.tax_value_ni = true;
				validated = false;
			}
		}

		return validated;
	};

	/**
	 * END SAVING THE PROPERTY
	 */

	/**
	 *	GETTING CEP
	 */

	$scope.street_disabled = true;
	$scope.district_disabled = true;

	$scope.get_address = function() {
		$scope.loading = true ;
		$scope.cep_not_found = false;
		$scope.cep_error = false;
		$scope.cep_not_informed = false;

		$scope.street_disabled = true;
		$scope.district_disabled = true;

		try {
			postal_code = $scope.property.location.postal_code;
		} catch (err) {
			$scope.cep_not_informed = true;
			$scope.loading = false;
			return;
		}

		if ( postal_code == "" ) {
			$scope.cep_not_informed = true;
			$scope.loading = false;
			return;
		}

		$http.get( '/correio/' + postal_code )
			.success( function( address ) {
				if ( angular.equals( address, {} ) ) {
					$scope.cep_not_found = true;
					$scope.loading = false;
					return;
				}

				console.log(address);
				if ( !address.address ) {
					$scope.street_disabled = false;
				}
				if ( !address.neighborhood ) {
					$scope.district_disabled = false;
				}

				$scope.property.location.street = address.address
				$scope.property.location.district = address.neighborhood
				$scope.property.location.city = address.city
				$scope.property.location.state = address.state
				$scope.loading = false;
			})
			.error( function( data ) {
				$scope.cep_error = true;
				$scope.loading = false;
			})
	}

	$scope.loading = false;
	$scope.cep_error = false;
	$scope.cep_not_found = false;
	$scope.cep_not_informed = false;
	/**
	 * END GETTING CEP
	 */

	/**
	 * DEFINIÇÃO DE CAMPOS POR TIPO DE IMÓVEL
	 **/

	$scope.options_type = ["Seleciona uma utilização"]

	$scope.define_types = function() {
		$scope.options_type = propertyKind( $scope.property.kind.utilization );
		console.log($scope.options_type);

		$scope.unit_details_per_utilization = [];
		for ( detail_index in $scope.unit_details ) {
			detail = $scope.unit_details[detail_index];
			utilizations = detail.utilizations;
			position = utilizations.indexOf( $scope.property.kind.utilization );

			if ( position != -1 ) {
				$scope.unit_details_per_utilization.push( detail );
			}
		}

		$scope.enterprise_details_per_utilization = [];
		for ( detail_index in $scope.enterprise_details ) {
			detail = $scope.enterprise_details[detail_index];
			utilizations = detail.utilizations;
			position = utilizations.indexOf( $scope.property.kind.utilization );

			if ( position != -1 ) {
				$scope.enterprise_details_per_utilization.push( detail );
			}
		}


		if ( $scope.property.kind.utilization == "RESIDENCIAL" ) {
			// Características
			$scope.disable_dormitories = false;
			$scope.disable_suites = false;
			$scope.disable_units_per_floor = false;
			$scope.disable_floors_number = false;
			$scope.disable_face = false;
			// Metragem
			$scope.disable_carpet_area = true;
			$scope.disable_office_area = true;
			$scope.disable_support_area = true;
			$scope.disable_patio_area = true;
			$scope.disable_manufacturing_area = true;
			$scope.disable_celing_height = true;
			$scope.disable_front_footage = true;
			$scope.disable_back_footage = true;
			$scope.disable_left_footage = true;
			$scope.disable_right_footage = true;

		}

		if ( $scope.property.kind.utilization == "COMERCIAL" ) {
			// Características
			$scope.disable_dormitories = true;
			$scope.disable_suites = true;
			$scope.disable_face = true;
			$scope.disable_units_per_floor = false;
			$scope.disable_floors_number = false;
			// Metragem
			$scope.disable_carpet_area = false;
			$scope.disable_office_area = false;
			$scope.disable_support_area = false;
			$scope.disable_patio_area = true;
			$scope.disable_manufacturing_area = true;
			$scope.disable_celing_height = false;
			$scope.disable_front_footage = false;
			$scope.disable_back_footage = false;
			$scope.disable_left_footage = false;
			$scope.disable_right_footage = false;
		}

		if ( $scope.property.kind.utilization == "INDUSTRIAL" ) {
			// Características
			$scope.disable_dormitories = true;
			$scope.disable_suites = true;
			$scope.disable_units_per_floor = true;
			$scope.disable_floors_number = false;
			$scope.disable_face = true;
			// Metragem
			$scope.disable_carpet_area = false;
			$scope.disable_office_area = false;
			$scope.disable_support_area = false;
			$scope.disable_patio_area = false;
			$scope.disable_manufacturing_area = false;
			$scope.disable_celing_height = false;
			$scope.disable_front_footage = false;
			$scope.disable_back_footage = false;
			$scope.disable_left_footage = false;
			$scope.disable_right_footage = false;
		}

		if ( $scope.property.kind.utilization == "RURAL" ) {
			// Características
			$scope.disable_dormitories = false;
			$scope.disable_suites = false;
			$scope.disable_units_per_floor = true;
			$scope.disable_floors_number = true;
			$scope.disable_face = true;
			// Metragem
			$scope.disable_carpet_area = true;
			$scope.disable_office_area = true;
			$scope.disable_support_area = true;
			$scope.disable_patio_area = true;
			$scope.disable_manufacturing_area = true;
			$scope.disable_celing_height = true;
			$scope.disable_front_footage = false;
			$scope.disable_back_footage = false;
			$scope.disable_left_footage = false;
			$scope.disable_right_footage = false;
		}

		$scope.validate();
	};
}

realtyExchangeApp.controller('PropertyCtrl', ['$scope', '$http', '$location', 'propertyKind', PropertyCtrl]);
