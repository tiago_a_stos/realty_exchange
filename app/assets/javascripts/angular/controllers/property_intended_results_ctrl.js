function PropertyIntendedResultsCtrl($scope, $http, $location, $routeParams, propertyKind, getMainPhoto) {
	$scope.params = $routeParams;

	console.log($scope.params.id);

	$scope.property = {};

	$http.get( '/user/signed_in' )
		.success( function( data ) {
			console.log( "Logado ");
		})
		.error( function( data ) {
			window.location = '/';
		});

	$scope.get_intended = function() {
		$http.get( '/property_intended/' + $scope.params.id )
			.success( function( data ) {
				console.log( data);
				$scope.property = data;
				$scope.get_properties();
			})
			.error( function( data ) {
				console.log( "Erro na requisição" )
			});
	}

	$scope.get_properties = function() {
		console.log("Buscando propriedades");
		area_total = "";
		area_util = "";
		if ( $scope.property.footage ) {
			if ($scope.property.footage.total_area ) {
				area_total = "&total_area_min=" + ( $scope.property.footage.total_area_min || "" ) + "&total_area_max=" + ( $scope.property.footage.total_area_max || "" );
			}
			if ($scope.property.footage.useful_area ) {
				area_util = "&useful_area_min=" + ( $scope.property.footage.useful_area_min || "" ) + "&useful_area_max=" + ( $scope.property.footage.useful_area_max || "" );
			}
		}

		$http.get( '/properties/search?state=' + ( $scope.property.location.state.id || "") +
							"&city=" + ( $scope.property.location.city.id || "" ) +
							"&district=" + ( $scope.property.location.district.id || "" ) +
							"&zone=" + ( $scope.property.location.zone || "" ) +
							"&utilization=" + ( $scope.property.kind.utilization || "" ) +
							"&type=" + ( $scope.property.kind.type || "" ) +
							"&dormitories_number=" + ( $scope.property.caracteristics.dormitories_number || "" ) +
							"&suites_number=" + ( $scope.property.caracteristics.suites_number || "" ) +
							"&parking_spaces=" + ( $scope.property.caracteristics.parking_spaces || "" ) +
							area_total + area_util )
			.success( function( properties ) {
				
				if ( properties.length < 1 ) {
					$scope.no_result = true;
				} else {
					$scope.no_result = false;
				}
			  $scope.properties = getMainPhoto( properties );
			});
	};

	$scope.get_intended();
	
}

realtyExchangeApp.controller('PropertyIntendedResultsCtrl', 
	['$scope', '$http', '$location', "$routeParams", 'propertyKind', 'getMainPhoto', PropertyIntendedResultsCtrl]);
