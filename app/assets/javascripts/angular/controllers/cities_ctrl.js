function CitiesCtrl($scope, $http) {
	$http.get( '/user/signed_in' )
		.success( function( data ) {
		})
		.error( function( data ) {
			window.location = '/';
		})

	console.log( "Cities Controller" );

	$scope.save_city = function() {
		console.log( "teste");
		var city = { "city": $scope.city }

		$http.post( "/city/", city )
			.success( function( city ) {
				$scope.get_cities();
			})
			.error( function( data ) {
				$scope.message = data.message;
			});
	}

	$scope.get_cities = function() {
		$http.get( '/cities/all' ).success( function( cities ) {
			$scope.cities = cities;
		});
	}

	$scope.get_states = function() {
		$http.get( '/states/all' ).success( function( states ) {
			console.log( states );
			$scope.states = states;
		});
	}

	$scope.get_cities();
	$scope.get_states();
}

realtyExchangeApp.controller('CitiesCtrl', ['$scope', '$http', CitiesCtrl]);
