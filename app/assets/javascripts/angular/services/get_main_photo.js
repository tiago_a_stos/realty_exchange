realtyExchangeApp.factory( 'getMainPhoto',['$http', function( $http ) {
  return function( properties ) {
		properties_with_photos = [];
    properties.forEach( function(element, index, array) {
      $http.get( '/photo/main/' + element.id )
        .success( function( photo ) {
          if ( photo != null ) {
            element["photo"] = photo;
          } else {
            element["photo"] = { "image": "/assets/casa.png" };
          }
          properties_with_photos.push( element );
        });
    });

    return properties_with_photos;
  };
}]);
