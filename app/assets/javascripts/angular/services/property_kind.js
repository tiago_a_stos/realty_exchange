realtyExchangeApp.factory( 'propertyKind', function() {
  return function( utilization ) {
    all_types = {
  	  "APARTAMENTO": ["RESIDENCIAL"],
  	  "AREA": ["COMERCIAL", "INDUSTRIAL"],
  	  "CASA ASSOBRADADA": ["RESIDENCIAL"],
  	  "CASA DE VILA": ["RESIDENCIAL"],
  	  "CASA TÉRREA": ["RESIDENCIAL"],
  	  "CHACARA": ["RURAL"],
  	  "COBERTURA": ["RESIDENCIAL"],
  	  "COMERCIAL": ["COMERCIAL"],
  	  "CONDOMÍNIO": ["RESIDENCIAL", "COMERCIAL", "INDUSTRIAL"],
  	  "CONJ. COMERCIAL": ["COMERCIAL", "INDUSTRIAL"],
  	  "DUPLEX": ["RESIDENCIAL"],
  	  "FAZENDA": ["RURAL"],
  	  "FLAT": ["COMERCIAL"],
  	  "GALPÃO": ["COMERCIAL", "INDUSTRIAL"],
  	  "LOJA": ["COMERCIAL", "INDUSTRIAL"],
  	  "PRÉDIO": ["RESIDENCIAL", "COMERCIAL", "INDUSTRIAL"],
  	  "SÍTIO": ["COMERCIAL"],
  	  "SOBRADO": ["RESIDENCIAL"],
  	  "TERRENO": ["INDUSTRIAL", "RURAL"],
  	  "TRIPLEX": ["RESIDENCIAL"]
  	};

  	options_type = [""];

  	for ( opt in all_types ) {
  		utilizations = all_types[opt];
  		position = utilizations.indexOf( utilization );

  		if ( position != -1 ) {
  			options_type.push( opt );
  		}
  	}

    return options_type;
  };
});
