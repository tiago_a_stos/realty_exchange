# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

#admin = User.create! :email => 'admin@realty.com.br', :password => '123456', :password_confirmation => '123456', :admin => true
#user = User.create! :email => 'usuario@realty.com.br', :password => '987654', :password_confirmation => '987654'

State.create! :name => 'Acre', :initial => 'AC'
State.create! :name => 'Alagoas', :initial => 'AL'
State.create! :name => 'Amapá', :initial => 'AP'
State.create! :name => 'Amazonas', :initial => 'AM'
State.create! :name => 'Bahia', :initial => 'BA'
State.create! :name => 'Ceará', :initial => 'CE'
State.create! :name => 'Brasília', :initial => 'DF'
State.create! :name => 'Espírito Santo', :initial => 'ES'
State.create! :name => 'Goiás', :initial => 'GO'
State.create! :name => 'Maranhão', :initial => 'MA'
State.create! :name => 'Mato Grosso', :initial => 'MT'
State.create! :name => 'Mato Grosso do Sul', :initial => 'MS'
State.create! :name => 'Minas Gerais', :initial => 'MG'
State.create! :name => 'Pará', :initial => 'PA'
State.create! :name => 'Paraíba', :initial => 'PB'
State.create! :name => 'Paraná', :initial => 'PR'
State.create! :name => 'Pernambuco', :initial => 'PE'
State.create! :name => 'Piauí', :initial => 'PI'
State.create! :name => 'Rio de Janeiro', :initial => 'RJ'
State.create! :name => 'Rio Grande do Norte', :initial => 'RN'
State.create! :name => 'Rio Grande do Sul', :initial => 'RS'
State.create! :name => 'Rondônia', :initial => 'RO'
State.create! :name => 'Rorâima', :initial => 'RR'
State.create! :name => 'Santa Catarina', :initial => 'SC'
State.create! :name => 'São Paulo', :initial => 'SP'
State.create! :name => 'Sergipe', :initial => 'SE'
State.create! :name => 'Tocantins', :initial => 'TO'

