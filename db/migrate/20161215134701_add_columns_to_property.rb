class AddColumnsToProperty < ActiveRecord::Migration[5.0]
  def change
    remove_column :properties, :title, :string
    rename_column :properties, :address, :location

    add_column :properties, :type, :jsonb
    add_column :properties, :construction, :jsonb
    add_column :properties, :caracteristics, :jsonb
    add_column :properties, :documentation, :jsonb
    add_column :properties, :footage, :jsonb
    add_column :properties, :values, :jsonb
  end
end
