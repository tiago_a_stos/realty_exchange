class AddColumnsToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :name, :string
    add_column :users, :last_name, :string
    add_column :users, :gender, :string
    add_column :users, :rg, :string
    add_column :users, :cpf, :string
    add_column :users, :marital_status, :string
    add_column :users, :alternative_email, :string
    add_column :users, :home_phone, :string
    add_column :users, :commercial_phone, :string
    add_column :users, :cell_phone, :string
    add_column :users, :cell_phone2, :string
  end
end
