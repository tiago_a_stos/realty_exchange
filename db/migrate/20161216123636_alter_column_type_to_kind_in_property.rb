class AlterColumnTypeToKindInProperty < ActiveRecord::Migration[5.0]
  def change
    rename_column :properties, :type, :kind
  end
end
