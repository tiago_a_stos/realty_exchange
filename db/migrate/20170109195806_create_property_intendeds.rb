class CreatePropertyIntendeds < ActiveRecord::Migration[5.0]
  def change
    create_table :property_intendeds do |t|
      t.string :title
      t.references :user, foreign_key: true
      t.jsonb :type
      t.jsonb :location
      t.jsonb :caracteristics
      t.jsonb :footage
      t.text :text

      t.timestamps
    end
  end
end
