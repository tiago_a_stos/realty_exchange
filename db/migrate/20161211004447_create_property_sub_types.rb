class CreatePropertySubTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :property_sub_types do |t|
      t.string :name
      t.string :description
      t.references :property_type, foreign_key: true

      t.timestamps
    end
  end
end
