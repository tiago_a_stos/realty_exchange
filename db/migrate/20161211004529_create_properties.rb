class CreateProperties < ActiveRecord::Migration[5.0]
  def change
    create_table :properties do |t|
      t.string :title
      t.references :user, foreign_key: true
      t.jsonb :address
      t.jsonb :details
      t.jsonb :exchange_options

      t.timestamps
    end
  end
end
