class AlterColumnTypeToKindInPropertyIntended < ActiveRecord::Migration[5.0]
  def change
    rename_column :property_intendeds, :type, :kind
  end
end
