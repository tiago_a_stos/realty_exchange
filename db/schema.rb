# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170110134441) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "cities", force: :cascade do |t|
    t.string   "name"
    t.integer  "state_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["state_id"], name: "index_cities_on_state_id", using: :btree
  end

  create_table "districts", force: :cascade do |t|
    t.string   "name"
    t.integer  "city_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["city_id"], name: "index_districts_on_city_id", using: :btree
  end

  create_table "photos", force: :cascade do |t|
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "property_id"
    t.index ["property_id"], name: "index_photos_on_property_id", using: :btree
  end

  create_table "properties", force: :cascade do |t|
    t.integer  "user_id"
    t.jsonb    "location"
    t.jsonb    "details"
    t.jsonb    "exchange_options"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.jsonb    "kind"
    t.jsonb    "construction"
    t.jsonb    "caracteristics"
    t.jsonb    "documentation"
    t.jsonb    "footage"
    t.jsonb    "values"
    t.index ["user_id"], name: "index_properties_on_user_id", using: :btree
  end

  create_table "property_categories", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "property_intendeds", force: :cascade do |t|
    t.string   "title"
    t.integer  "user_id"
    t.jsonb    "kind"
    t.jsonb    "location"
    t.jsonb    "caracteristics"
    t.jsonb    "footage"
    t.text     "text"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["user_id"], name: "index_property_intendeds_on_user_id", using: :btree
  end

  create_table "property_models", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "property_sub_types", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "property_type_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["property_type_id"], name: "index_property_sub_types_on_property_type_id", using: :btree
  end

  create_table "property_types", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "states", force: :cascade do |t|
    t.string   "name"
    t.string   "initial"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.boolean  "admin",                  default: false
    t.string   "name"
    t.string   "last_name"
    t.string   "gender"
    t.string   "rg"
    t.string   "cpf"
    t.string   "marital_status"
    t.string   "alternative_email"
    t.string   "home_phone"
    t.string   "commercial_phone"
    t.string   "cell_phone"
    t.string   "cell_phone2"
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  add_foreign_key "cities", "states"
  add_foreign_key "districts", "cities"
  add_foreign_key "photos", "properties"
  add_foreign_key "properties", "users"
  add_foreign_key "property_intendeds", "users"
  add_foreign_key "property_sub_types", "property_types"
end
