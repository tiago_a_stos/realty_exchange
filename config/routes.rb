Rails.application.routes.draw do

  get 'correio/:postal_code' => 'correio#get_address'

  get 'photo/main/:property_id' => 'photo#main_photo'

  resources :property_intended, :only => [:create, :show]
  get 'properties_intended/my' => 'property_intended#list_by_user'

  resources :property, :only => [:create, :show]
  get 'properties/my' => 'property#list_by_user'
  get 'properties/all' => 'property#list'
  get 'properties/photos/:id' => 'property#photos'
  post 'properties/photo/:id' => 'property#photo_upload'
  get 'properties/search' => 'property#search'

  resources :district, :only => [:create]
  get 'districts/all' => 'district#list'
  get 'districts/by_city/:city_id' => 'district#get_by_city'

  resources :city, :only => [:create]
  get 'cities/all' => 'city#list'
  get 'cities/by_state/:state_id' => 'city#get_by_state'

  resources :state, :only => [:create]
  get 'states/all' => 'state#list'

  get 'user/signed_in' => 'user#signed_in?'

  #devise_for :users
  devise_for :users, controllers: { sessions: 'user/sessions', registrations: 'user/registrations' } do
     post '/users/sign_in' => 'sessions#create'
     post '/users' => 'registrations#create'
  end

  root "home#index"
  get "*path" => redirect("/")
end
